// Конструкція try...catch дозволяє ловити помилки, що зручно 
// для розробника, щоб код "не падав", довзоляє виводити ці помилки 
// користувачеві для розуміння останнього чому неможливо виконати
// ту чи іншу дію, при роботі з базами данних, файлами тощо.

const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];


function createList(books){
    const root = document.querySelector("#root");
    const ul = document.createElement("ul");
    root.append(ul);

        books.forEach(book => {
        const li = document.createElement("li");
        li.innerHTML = `Автор: ${book.author}<br>
                        Назва: ${book.name}<br>
                        Ціна: ${book.price}`;

        if (typeof book.author === "undefined" || typeof book.name === "undefined" || typeof book.price === "undefined") {
            console.error(`Помилка: відсутні властивості ${JSON.stringify(book)}`);
            return
        }

        ul.append(li);
    });
}

try{
    createList(books)
} catch(error){
    console.log(`${error.name} : ${error.message}`)        
} 





